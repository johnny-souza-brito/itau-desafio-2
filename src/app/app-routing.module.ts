import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { LancamentosComponent } from './components/lancamentos/lancamentos.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'lancamentos' },
    { path: 'lancamentos', component: LancamentosComponent }
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
